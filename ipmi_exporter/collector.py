import logging
from concurrent.futures import ThreadPoolExecutor, as_completed

from prometheus_client.core import GaugeMetricFamily

from .exporter import Exporter


class IPMICollector:
    __slots__ = ('exporter', 'logger')

    def __init__(self, command, host, username, password):
        self.exporter = Exporter(command, host, username, password)
        self.logger = logging.getLogger(__name__)

    def get_metrics(self):
        subcommands = (
            self.exporter.pminfo,
            self.exporter.power_status,
            self.exporter.sensors,
        )

        with ThreadPoolExecutor(max_workers=3) as executor:
            futures = {
                executor.submit(subcommand) for subcommand in subcommands
            }

            for subcommand in as_completed(futures):
                yield from subcommand.result()

    def collect(self):
        for metric in self.get_metrics():
            self.logger.debug(metric)
            yield GaugeMetricFamily(
                metric.name, metric.help, value=metric.value
            )
